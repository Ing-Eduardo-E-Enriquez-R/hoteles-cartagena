module.exports = function(grunt) {
     grunt.initConfig({
          sass: {
               dist: {
                    files: [{
                         expand: true,
                         cwd: 'css',
                         src: ['*.scss'],
                         dest: 'css',
                         ext: '.css'
                    }]
               }
          },
          watch: {
              files: ['css/*.scss'],
              tasks: ['css']
         },
         browserSync: {
              dev: {
                   bsFiles: {
                        src: [
                             'css/*.scss',
                             '*.html',
                             'js/*.js'
                        ]
                   },
                   options: {
                        watchTask: true,
                        server: {
                             baseDir: './'
                        }
                   }
              }
         },
         imagemin: {
             static: {
                 options: {
                     optimizationLevel: 3,
                     svgoPlugins: [{removeViewBox: false}],
                     use: [mozjpeg()] // Example plugin usage
                 },
                 files: {
                     'dist/img.png': 'src/img.png',
                     'dist/img.jpg': 'src/img.jpg',
                     'dist/img.gif': 'src/img.gif'
                 }
             },
             dynamic: {
                 files: [{
                     expand: true,
                     cwd: './',
                     src: ['img/*.{png,jpg,gif}'],
                     dest: 'dist/'
                 }]
             }
        },
     });
     grunt.loadNpmTasks('grunt-contrib-watch');
     grunt.loadNpmTasks('grunt-contrib-sass');
     grunt.loadNpmTasks('grunt-contrib-imagemin');
     grunt.registerTask('default', ['imagemin']);
     grunt.registerTask('css', ['sass']);
     grunt.registerTask('default', ['browserSync', 'watch']);
};
